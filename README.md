# Snake
This is a Haskell implementation of the classic game of Snake mostly known from old Nokia phones.
You can control the Snake body using the Arrow Keys. Every time the Snake head hits a dark dot, it's
body length is increased by one dot. You die if the snake hits it's own body, press space to restart 
the game. The goal of the game is to collect as much dots as possible.

#### how to build
Clone the git repo and run 
> stack build

to also run the game you can run
> stack build; stack exec snake-exe

#### Current ~~Bugs~~ Features:
- dots can spawn in the snake body
- changing directions to fast can result in death
  (maybe use a event list or similar)
 
#### Todo
- make GameState a record type
