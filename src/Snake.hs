{-# LANGUAGE DeriveFunctor #-}

module Snake
    ( Cord
    , Direction (..)
    , Snake (..)
    , s
    , dirCord
    , cordSnake
    , getHead
    , move
    , snap
    , contains
    , cordDir
    , red
    , moveM
    , s2c
    , snake2list
    ) where

type Cord = (Int,Int)
data Direction = North | East | South | West deriving (Show, Eq)
newtype Snake a = Snake (a, [a]) deriving (Show, Functor)

move :: Direction -> Snake [Direction] -> Snake [Direction]
move dir (Snake (a, as)) = Snake (f (dir:a), a:as)
        where f = red

moveM :: Direction -> Snake [Direction] -> Maybe (Snake [Direction])
moveM dir s = if b then Nothing else Just news'
    where
        news' = move dir s
        cords' = cordSnake dirCord <$> news'
        cords  = cordSnake dirCord <$> s
        head = getHead cords'
        b = contains head cords



cordDir :: Cord -> [Direction]
cordDir (x,y)
    | x < 0 && y < 0 = replicate (-x) North ++ replicate (-y) West
    | x < 0          = replicate (-x) North ++ replicate y East
    | y < 0          = replicate x South ++ replicate (-y) West
    | otherwise      = replicate x South ++ replicate y East

s :: Snake [Direction]
s = Snake ([], [])

contains :: Eq a => a -> Snake a -> Bool
contains a (Snake (x, xs)) = a == x || elem a xs

getHead :: Snake a -> a
getHead (Snake (a,_)) = a

redWith :: (Direction -> Cord -> Cord) -> [Direction] -> [Direction]
redWith f = (c2d . d2c)
        where c2d = cordDir
              d2c = cordSnake f

red :: [Direction] -> [Direction]
red = redWith dirCord


snap :: Int -> Snake a -> Snake a
snap i (Snake (a, as)) = Snake (a, take (i-1) as)

dirCord :: Direction -> Cord -> Cord
dirCord North (x,y) = (mod (x-1) 20,y)
dirCord South (x,y) = (mod (x+1) 20,y)
dirCord East  (x,y) = (x,mod (y+1) 20)
dirCord West  (x,y) = (x,mod (y-1) 20)

cordSnake :: (Direction -> Cord -> Cord) -> [Direction] -> Cord
cordSnake f = foldr f (0,0)

s2c :: Snake [Direction] -> Snake Cord
s2c s = cordSnake dirCord <$> s

snake2list :: Snake a -> [a]
snake2list (Snake (a,as)) = a:as