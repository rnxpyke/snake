module Main where

import Snake
import System.Random
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game

main :: IO ()
main = do 
    a <- randomRsIO (0,19)
    b <- randomRsIO (0,19)
    let c  = zip a b
    game c

--old list
--sequence $ replicate n $ rt (0,19)

--thx stack overflow:
randomRsIO b = randomRs b <$> newStdGen

--GameState :: list of dots, current direction, length, snake, isRunning
type GameState = ([Cord], Direction, Int, Snake [Direction], Bool)

--Create the start state based on the dot positions
start :: [Cord] -> GameState
start x = (x, North, 3, s, True)

--run the game with gloss
game x = play window white 10 (start x) (fix .render) getCommand step

window = InWindow "MyWindow" (400,400) (1000,1000)
fullscreen = FullScreen 

--center game frame
fix :: Picture -> Picture
fix =  translate (-190) 190

render :: GameState -> Picture
render (a, _, _, s, _) = Pictures [renderSnake s, renderCord $ head a]

renderSnake :: Snake [Direction] -> Picture
renderSnake s = color (makeColorI 68 68 68 255) $ Pictures $ renderCord <$> (snake2list snake)
    where snake = s2c s

renderCord :: Cord -> Picture
renderCord (a,b) = translate y (-x) $ circleSolid 10
    where x = 20 * fromIntegral a
          y = 20 * fromIntegral b


--Game operations

getDir :: GameState -> Direction
getDir (_, dir, _ ,_, _) = dir

setDir :: Direction -> GameState -> GameState
setDir dir (a, _, b, c, d) = (a, dir, b, c, d)

setIf :: Direction -> Direction -> GameState -> GameState
setIf check dir s = if (getDir s) == check then s else setDir dir s 

reset :: GameState -> GameState
reset (a,_,_,_, False) = start a
reset x = x 

--event handler
getCommand :: Event -> GameState -> GameState
getCommand (EventKey (SpecialKey KeyUp) Down _ _) = setIf South North
getCommand (EventKey (SpecialKey KeyDown) Down _ _)  = setIf North South
getCommand (EventKey (SpecialKey KeyLeft) Down _ _) = setIf East West
getCommand (EventKey (SpecialKey KeyRight) Down _ _)  = setIf West East
getCommand (EventKey (SpecialKey KeySpace) Down _ _) = reset
getCommand _ = id

mtest :: Maybe a -> Bool
mtest (Just _) = True
mtest Nothing = False


--calculate one step of the game
step :: Float -> GameState -> GameState
step _ (a, dir, l, s, t) = if isLegal then (a', dir, l', next, t) else (a, dir, l, s, False)
    where 
        move' = move dir s
        isLegal = t && (mtest $ moveM dir s)
        headC = cordSnake dirCord $ getHead move'
        b = head a == headC
        l' = if b then l+1 else l
        next = snap l' move'
        a' = if b then tail a else a
